from django.shortcuts import refrom, render 

from django.http import HttpResponse
from django.views import View


from .forms import Encuestas_cliente

# Create your views here.


#Vista de formulario: Armar el formulario.
class ViewEncuestas_cliente(View):
    
    def renderEncuestas_cliente(request):
        #Crear un objeto de la clase del formulario 
        
        formularioEncuestas_cliente= Encuestas_cliente()
        
        #Diccionario de datos
        
        datos={
            "form": formularioEncuestas_cliente  
        }
        
        miFormulario=render(request,"FormEncuestas_cliente.html", datos)
        
        return miFormulario
    
    

#Vista provicional, no sé qué nombre ponerle. 
class Nose(View):
    
    def get (self, request):
        a= HttpResponse('Bienvenido a la app creada para brindarle seguridad')
        return a 